# Discord Bot Dev 1

## Challenge files
None. We have to find the author's github lol

## Analysis
I found the repo [here](https://github.com/nbanmp/ccc-discord-bot-dev). There's a commit called [Removed discord server](https://github.com/nbanmp/ccc-discord-bot-dev/commit/5190c53ca0ba633b39af98508254c5b2059a8500) that has a discord invite. I joined the server.

I read the bot source code, and the first flag is sent to the `#flag-deletion-test` channel once every 15 minutes. The problem is, the bot is also programmed to detect and delete flags, so the message is deleted before we can see it.

## Solution
Discord notifications on mobile don't go away when the message is deleted. This might be cheese.
![screenshot of the notification](phone.png)
