# angrbox

## Challenge files
```
Write me a program that:
- Takes 4 uppercase characters in argv
- Verifies the 4 character key and returns 0 if correct
- If I find the key, YOU LOSE
```

We're also given the server source code which contains a proof of work challenge. The problem itself is pretty self-explanatory though, because all it does is run angr on the program and see if it can solve it.

## Analysis
```py
def send_pow(req: MyTCPHandler):
    pool = string.ascii_letters + string.digits
    s = "".join(random.choices(pool, k=16))
    h = hashlib.sha256(s.encode()).hexdigest()
    prefix = s[:4]
    suffix = s[4:]
    req.wfile.write(f"[*] suffix = {suffix}\n".encode())
    req.wfile.write(f"[*] sha256 = {h}\n".encode())
    req.wfile.write(f"[*] Give me the {len(prefix)} character prefix: ".encode())
    p = req.rfile.readline().strip().decode("ascii", errors="ignore")
    if p == prefix:
        return True
    else:
        req.wfile.write(f"[-] That's wrong\n".encode())
        return False
```

The proof of work function generates 16 random characters, and gives us the sha256 hash of the characters as well as the last 12 characters. We just have to brute force the first 4.

## Solution
Proof of work code:

> pow.py
```py
import hashlib
import string
import random
import itertools

def pow(hsh, suffix):
    pool = string.ascii_letters + string.digits
    for s in itertools.product(pool, repeat=4):
        s = "".join(s) + suffix
        h = hashlib.sha256(s.encode()).hexdigest()
        if h == hsh:
            return s[:4]
```

To defeat angr, we just need something with lots of conditional branches so that it runs out of memory. Here's what I came up with:

```c
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

char* encode(char* code) {
	static char result[257];
	for (int i = 0; i < 256; i++) {
		result[i] = code[i % 4];
	}
	for (int i = 0; i < 1000000; i++) {
		unsigned r = rand();
		char rs[4];
		memcpy(rs, &r, 4);
		for (int j = 0; j < 64; j++) {
			unsigned idx = rand() % 256;
			for (int k = 0; k < 4; k++) {
				result[idx+k] ^= rs[k];
			}
		}
		// have fun with this one angr
		if (r < 1 << 4) i--;
	}
	result[256] = '\0';
	return result;
}

int main(int argc, char* argv[]) {
	srand(0xfeedbeef);
	if (argc <= 1) return 1;
	char* expected = "\x29\xd4\x6b\xc5\x01\x87\xda\xa1\x6c\x36\x23\xcd\x5c\x5d\xcb\xd6\xb4\x98\xd7\xa9\x26\x0e\xbb\xd1\x21\xdc\x7a\x7d\xd9\xb2\x99\x9e\xa4\x98\x83\xb7\x50\xfc\x37\x49\x25\x14\xf7\x10\x49\x0a\x37\x77\x54\xb0\x8f\xb3\x47\xba\x4f\x0a\x2d\x9d\xda\x8b\xe6\x29\xba\xcd\xbb\x23\x4d\xf6\x03\x2e\x7a\x6e\x7c\x11\x24\x37\xc5\x6e\x31\x0f\x52\xc1\x4a\x76\xfe\xec\x83\x65\x17\xc3\xe8\xe4\x4c\x05\xfb\xec\x53\xa2\x27\xf4\x9c\x4f\x84\x07\xa9\xa2\x74\x8a\x47\xcf\x20\xed\x61\xa2\x7e\xcc\xec\x71\x97\xa3\x26\x29\x2f\x0b\xcb\xaf\x25\xa3\xa2\x5b\xc4\x12\xfb\xcd\x32\x36\x7d\x4e\x12\xdd\x4a\x9c\xd3\xbd\x79\x0c\x7a\xbe\x79\x6a\xdd\x56\x33\xc2\xa2\x24\x31\x7c\x4c\x2c\x77\xd3\xdc\xb2\xb0\x9f\x38\xe0\x87\x50\x1d\xd7\x12\x14\x27\x70\x74\xf3\x2d\xac\x2e\x99\x5c\xc6\xcf\x08\xc2\xdc\xad\x38\x5e\x46\xbb\x85\xb8\x0b\xa5\x80\x85\x76\x58\xba\xf7\xe3\x29\xa4\x3d\xf7\xf1\x3f\x6f\xf7\x4b\x94\xfc\x78\x8a\x9c\x3b\x32\xeb\x06\xc1\xa4\x11\x45\x31\x39\x81\xab\x4d\xef\x7a\x86\x73\xb1\x3a\xa9\xc6\x47\xda\x8e\x8e\x90\x22\xea\x10\x57\xbb\x88\xa7\x7b\xf9\xf8\x25\x5e";
	char* actual = encode(argv[1]);
	return strcmp(actual, expected);
}
```

Then we just need code to send it along, wait 2 minutes, and we've got the flag.

```py
from pwn import *
import pow
import codecs

conn = remote("35.194.4.79", 7000)
suffix = conn.recvline(keepends=False).decode("ascii").removeprefix("[*] suffix = ")
print(suffix)
hsh = conn.recvline(keepends=False).decode("ascii").removeprefix("[*] sha256 = ")
print(hsh)
res = pow.pow(hsh, suffix)
conn.sendline(res.encode("ascii"))
conn.recvline()
conn.recvline()
conn.recvline()
conn.recvline()
conn.recvline()
conn.recvline()
conn.recvline()
conn.recvline()
with open("./angrbox.c", "rb") as f:
    conn.sendline(codecs.encode(f.read(), "hex"))
conn.interactive()
```
