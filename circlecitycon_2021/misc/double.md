# Double

## Challenge files
We're given a Volatility profile and a memory dump.

## Analysis
Running vol's `linux_find_file` on the dump gives us some interesting files:
```
          289058 0xffffa0f6fadd78c8 /var/lib/docker/overlay2/0302e6c324b486a627e0243c020d8a7d5edd1eab9f186af5d0f6a83b5b82c989/diff/secret.txt
----------------                0x0 /var/lib/docker/overlay2/0302e6c324b486a627e0243c020d8a7d5edd1eab9f186af5d0f6a83b5b82c989-init/diff/secret.txt
----------------                0x0 /var/lib/docker/overlay2/c6010ae8b5857ab4d731cead4147b7d55b6ed8f985d5cbd975cfa529d2d75e30/diff/secret.txt
          289058 0xffffa0f6fa99bb40 /var/lib/docker/overlay2/0302e6c324b486a627e0243c020d8a7d5edd1eab9f186af5d0f6a83b5b82c989/merged/secret.txt
```

## Solution
Extracting that file gives us the flag.
```sh
$ vol.py --profile=LinuxUbuntu_5_4_0-62-generic_profilex64 -f dump.mem linux_find_file -i 0xffffa0f6fadd78c8 -O secret2.txt
Volatility Foundation Volatility Framework 2.6.1
$ cat secret2.txt
C C C { d 0 c k 3 r _ i n _ a _ V M }
```

If we want to get fancy we can delete the stupid spaces.
```sh
$ cat secret2.txt | tr -d ' '
CCC{d0ck3r_in_a_VM}
```
