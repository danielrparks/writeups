# Discord Bot Dev 2

> Note: this writeup is done from memory. It will be sparse compared to the others.

## Challenge files
This is the same bot as in the previous challenge, we just have to find the second flag.

## Analysis
The bot is programmed to give you the flag only if you're an admin in the author's server. It also has a feature where you can send a reminder anywhere, with any text. Well, almost anywhere. It won't let you send it to the author's server.

## Solution
You can actually generate a discord api url to add any bot to your own server.
I did that, told it to send the reminder with `sudo debug xxxxxxxx` (the command to get the flag). Then I did binary search with the number of x's to find the flag length, and finally xored the encoded flag with the x's again to get the plain text flag.
