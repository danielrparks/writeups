# Artform
## Challenge files
another binary!!

## Analysis
basically ghidra lol
```c
undefined8 main(void)

{
  long in_FS_OFFSET;
  undefined8 local_38;
  undefined8 local_30;
  undefined8 local_28;
  undefined8 local_20;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  local_28 = 0x625f3368745f7434;
  local_20 = 0x7d68357572;
  local_38 = 0x316c5f317b434343;
  local_30 = 0x33625f30745f336b;
  memset(&local_38,0x41,0x20);
  printf("You like to paint? You know what I say to that? %s!",&local_38);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return 0;
}
```

Looks like it has the flag in memory, but destroys it with memset.

## Solution
We could manually decode the hex data to get the flag, but it's easier to just skip the memset call in gdb.
