# Lonk

## Challenge files
A python script and the module it imports. All the function names are unicode characters that don't render with the fonts I have installed.

## Analysis
`flag.py` begins printing out the flag when run, but slows to a crawl. It will also run out of memory if you let it run long enough (I totally didn't learn that the hard way).

I renamed all the functions using my ide (well, nvim + coc + coc-python) so that I could actually tell them apart. There's also a class.

`flag.py` is really hairy, so I started with `lib.py`.

The first thing I noticed was that the class was used to store ints as the length of linked lists (that's why it's running out of memory):
```py
class packobj:
    def __init__(self, n=None):
        self.n = n


def packint(a):
    h = packobj()
    c = h
    while a > 0:
        c.n = packobj()
        c = c.n
        a -= 1
    return h.n


def unpackint(a):
    i = 0
    while a:
        i += 1
        a = a.n
    return i
```

Manually reversing the rest of the library, I found functions for addition, subtraction, multiplication, modulus, power, printing integers as characters, and some kind of transformation involving modulus and pow:

```py
def copyofint(a):
    h = packobj()
    b = h
    while a:
        b.n = packobj()
        b = b.n
        a = a.n
    return h.n


def msum(a, b):
    h = copyofint(a)
    c = h
    while c.n:
        c = c.n
    c.n = copyofint(b)
    return h


def msub(a, b): # subtracts b from a
    h = copyofint(a)
    c = h
    d = b
    while d:
        c = c.n
        d = d.n
    return c


def mmult(a, b):
    h = packobj()
    c = a
    while c:
        h = msum(h, b)
        c = c.n
    return h.n


def mmod(a, b):
    r = copyofint(b)
    # c = b * 2
    c = r
    while c.n:
        c = c.n
    c.n = r
    # end

    d = r # d = b * 2
    c = a # c = a
    # d = d - a
    while c.n:
        d = d.n
        c = c.n
    # end

    if id(d.n) == id(r): # 
        r = None
    else:
        d.n = None

    return r


def mpow(a, b):
    h = packobj()
    c = b
    while c:
        h = mmult(h, a)
        c = c.n
    return h


def unktrans(a, b, m):
    return mmod(mpow(a, b), m)


def printchar(n):
    print(chr(unpackint(n)), end="", flush=True)
```

Looking at `flag.py`, it's clear why it's taking so long:

```py
printchar(unktrans(packint(2020), packint(451813409), packint(2350755551)))
```

So I reimplemented `lib` using proper math and the mod library, which I installed using pip:
```py
from mod import Mod


def packint(a):
    return a


def unpackint(a):
    return a


def copyofint(a):
    return a


def msum(a, b):
    return a + b


def msub(a, b): # subtracts b from a
    return a - b


def mmult(a, b):
    return a * b


def mmod(a, b):
    return a % b


def mpow(a, b):
    return a ** b


def mpowmod(a, b, m):
    return int(Mod(a, m) ** b)

def printchar(n):
    print(chr(unpackint(n)), end="", flush=True)
```

Now the flag prints out in a fraction of a second.
