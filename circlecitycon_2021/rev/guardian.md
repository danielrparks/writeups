# Guardian

## Challenge files
Just another binary!

## Analysis
```c
undefined8 main(void)

{
  long lVar1;
  char *flag;
  size_t flaglen;
  char *inbuf;
  char *retval;
  ulong i;
  long in_FS_OFFSET;
  
  lVar1 = *(long *)(in_FS_OFFSET + 0x28);
  boringsetup();
  flag = getflag();
  if (flag != (char *)0x0) {
    flaglen = strlen(flag);
    inbuf = (char *)calloc(1,flaglen + 2);
    if (inbuf != (char *)0x0) {
      __printf_chk(1,"%s\n\nHOOOOOOOOOO Goes there? Do you have the password?\n> ",owl);
      retval = fgets(inbuf,(int)flaglen + 1,stdin);
      if (retval != (char *)0x0) {
        if (flaglen != 0) {
          i = 0;
          do {
            while( true ) {
              if (inbuf[i] != flag[i]) {
                puts("\nHoo hoo hoo!\nThat is incorrect, my guardian.");
                goto die;
              }
              i = i + 1;
              __printf_chk(1,L"鳢₅ ");
              if ((i & 7) != 0) break;
              putchar('\n');
              if (i == flaglen) goto done;
            }
          } while (i != flaglen);
        }
done:
        puts("\nWe will do our best.....you have fought well.");
        if (lVar1 == *(long *)(in_FS_OFFSET + 0x28)) {
          return 0;
        }
                    /* WARNING: Subroutine does not return */
        __stack_chk_fail();
      }
    }
  }
die:
                    /* WARNING: Subroutine does not return */
  exit(0);
}
```

## Solution
The unicode got messed up somewhere, but the gist of it is that it prints check marks for every character of the flag that we get right. So, we can write a script to brute force each character indivdidually.

```py
from pwn import *
import string

answer = "CCC{"
alphabet = string.ascii_letters + string.digits + string.punctuation + " "
alphaiter = iter(alphabet)
prevlen = len(answer)

while answer[-1] != "}":
    conn = remote("35.224.135.84", 2000)
    while not b"password" in conn.recvline():
        pass
    thistry = answer + alphaiter.__next__()
    print(thistry)
    conn.sendline(thistry)
    thisline = conn.recvline()
    checks = thisline
    while len(thisline) >= 41:
        thisline = conn.recvline()
        checks += thisline
    checks = checks.decode("utf-8")
    print(checks)
    thislen = checks.count("✅")
    print(thislen)
    if thislen > prevlen:
        prevlen = thislen
        answer = thistry
        print(answer)
        alphaiter = iter(alphabet)
    conn.close()
```
