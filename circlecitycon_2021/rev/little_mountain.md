# Little Mountain

## Challenge files
Just another binary

## Analysis
When I initially opened it in ghidra, it was full of garbage and the entry point was nonsense. I was about to give up, but I remembered to try extracting it with UPX. Surprisingly, that worked and gave me a much cleaner binary. (Normally I would have noticed earlier because the `file` command can detect it, but for some reason it didn't detect it this time.)

Ghidra gave a nice decompilation of the main function:
```c
void main(void)

{
  int choice;
  
  setabuf();
  do {
    puts("Option 0: Guess the number");
    puts("Option 1: Change the number");
    puts("Option 2: Exit");
    __isoc99_scanf("%d",&choice);
    (*(code *)funcs[choice])();
  } while( true );
}
```

The `funcs` array is initialized by `setabuf`, which I also decompiled:
```c
void setabuf(void)

{
  srandom(0x539);
  magic = random();
  funcs[0] = opt0;
  funcs[1] = opt1;
  funcs[2] = opt2;
  funcs[3] = opt3hidden;
  return;
}
```

There's a hidden fourth option! (my names)

The decompilation of the hidden function is (my variable names):
```c

void opt3hidden(void)

{
  byte out;
  int name_len;
  int encrypted_len;
  char *name;
  char *encrypted;
  int j;
  int i;
  
  encrypted = "\n\x05\x15\x13\x17\ak\x0f\x16\x06YG\x11\\\x1b\x1c\x1dG\x1c\x01U*\x03XA_\x1a\x1c";
  name = "little_mountain";
  encrypted_len =
       strlen?("\n\x05\x15\x13\x17\ak\x0f\x16\x06YG\x11\\\x1b\x1c\x1dG\x1c\x01U*\x03XA_\x1a\x1c");
  name_len = strlen?(name);
  j = 0;
  if (modded == 0x14) {
    i = 0;
    while (i < encrypted_len) {
      if (j == name_len) {
        j = 0;
      }
      out = name[j] ^ encrypted[i];
      j = j + 1;
      write(1,&out,1);
      i = i + 1;
    }
    puts("\n");
  }
                    /* WARNING: Subroutine does not return */
  exit(0);
}
```

So this function only activates if we set `modded` to 20. The `opt1` function increments it:
```c
void opt1(void)

{
  puts("Always ready for more");
  magic = random();
  modded = modded + 1;
  return;
}
```

## Solution
So we just choose 1 twenty times, and then 3. Then it just prints out the flag.

We could have also manually decrypted it given the encrypted string and the key.
